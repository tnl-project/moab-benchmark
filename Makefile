CXX := g++

CPPFLAGS := -MD -MP -I ./libs/install/include
CXXFLAGS := -std=c++11 -Wall -Wextra -pedantic -O3 -march=native -mtune=native -flto -fopenmp
LDLIBS := -lm -lMOAB -lgomp
LDFLAGS := -flto

# add the path where MOAB was installed to linker flags
ifneq ("$(wildcard ./libs/install/lib/*)", "")
LDFLAGS += -L ./libs/install/lib -Wl,-rpath,"$(CURDIR)/libs/install/lib"
endif
ifneq ("$(wildcard ./libs/install/lib64/*)", "")
LDFLAGS += -L ./libs/install/lib64 -Wl,-rpath,"$(CURDIR)/libs/install/lib64"
endif

SOURCES = $(wildcard *.cpp)
TARGETS = $(SOURCES:%.cpp=%)

all: $(TARGETS)

.PHONY: moab
moab:
	+make -C libs moab

%.o: %.cpp moab
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c -o $@ $<

$(TARGETS): %: %.o
	$(CXX) $(LDFLAGS) -o $@ $^ $(LDLIBS)

.PHONY:
clean:
	$(RM) *.[od] $(TARGETS)

.PHONY:
distclean: clean
	+make -C libs clean

-include $(SOURCES:%.cpp=%.d)
