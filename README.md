# MOAB benchmark

These algorithms (`moab-benchmark-measures` and `moab-benchmark-surfaces`) were used for comparison
with the [TNL mesh benchmarks](https://gitlab.com/tnl-project/tnl-benchmark-mesh) in the paper
[Configurable open-source data structure for distributed conforming unstructured homogeneous meshes
with GPU support](https://doi.org/10.1145/3536164).

## Quickstart

1. Make sure that [Git](https://git-scm.com/), [Git LFS](https://git-lfs.github.com/),
   [GNU Make](https://www.gnu.org/software/make/), [CMake](https://cmake.org/),
   [GNU Compiler Collection](https://gcc.gnu.org/), and [Eigen](https://eigen.tuxfamily.org/)
   are installed on your Linux system.

2. Clone the repository and initialize the submodules:

       git clone --recurse-submodules https://gitlab.com/tnl-project/moab-benchmark.git

3. Build the benchmark binaries:

       cd moab-benchmark
       make

4. Run the script to execute the benchmarks on the meshes included in the repository:

       ./run_benchmark
